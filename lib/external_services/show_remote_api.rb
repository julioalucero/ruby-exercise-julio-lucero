module ExternalServices
  class ShowRemoteApi
    URL = 'https://shows-remote-api.com'.freeze

    def call
      @response ||= JSON.parse(get_request.body)
    end

    private

    def get_request
      Faraday.get(URL)
    end
  end
end
