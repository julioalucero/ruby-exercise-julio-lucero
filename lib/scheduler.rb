class Scheduler
  SECONDS_SINCE = 3600
  DEFAULT_SECONDS_INTERVALS = 15

  attr_reader :shows

  def initialize
    @shows = shows_to_be_updated
  end

  def call
    shows.each_with_index.with_object({}) do |(show, index), result|
      result[show.id] = (index * interval).round
    end
  end

   private

  def response
    @response ||= ExternalServices::ShowRemoteApi.new.call
  end

  def shows_to_be_updated
    Show.last_updates_since(SECONDS_SINCE).reject do |show|
      show.quantity == find_response_quantity(show.id)
    end
  end

  def find_response_quantity(id)
    show = response.find { |show_hash| show_hash['id'] == id }

    show&.dig('quantity')
  end

  def interval
    exceeds_shows_from_period ? interval_split_in_period : DEFAULT_SECONDS_INTERVALS
  end

  def exceeds_shows_from_period
    shows.count > SECONDS_SINCE / DEFAULT_SECONDS_INTERVALS
  end

  def interval_split_in_period
    SECONDS_SINCE.to_f / shows.count
  end
end
