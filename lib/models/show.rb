require "active_record"

class Show < ActiveRecord::Base
  scope :last_updates_since,-> (seconds) { where('last_update IS NULL OR last_update >= ?', seconds) }
end
