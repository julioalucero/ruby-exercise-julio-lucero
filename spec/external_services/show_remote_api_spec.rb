RSpec.describe ExternalServices::ShowRemoteApi do
  subject { described_class.new.call }

  describe '#call' do
    let(:faraday_response) { [{"id"=>1, "quantity"=>15}, {"id"=>2, "quantity"=>3}, {"id"=>3, "quantity"=>40}, {"id"=>4, "quantity"=>22}] }

    context "example 1", vcr: { cassette_name: "example1" } do
      let(:example_file) { "spec/fixtures/example1.txt" }

      it 'makes a GET request to the correct URL' do
        expect(subject).to eq(faraday_response)
      end
    end
  end
end
