RSpec.describe Scheduler do
  before do
    # Seed database
    File.read(example_file).each_line do |line|
      id, quantity, last_update = line.split(",")
      Show.create!(id: id, quantity: quantity, last_update: last_update)
    end
  end

  subject { described_class.new.call }

  context "example 1", vcr: { cassette_name: "example1" } do
    let(:example_file) { "spec/fixtures/example1.txt" }
    let(:needs_updating) { [1, 2] }
    let(:scheduled) { { 1 => 0, 2 => 15 } }

    it "finds the IDs that need updating" do
      expect(subject.keys).to match_array needs_updating
    end

    it "creates the update schedule" do
      expect(subject).to eq(scheduled)
    end
  end

  context "example 2", vcr: { cassette_name: "example2" } do
    let(:example_file) { "spec/fixtures/example2.txt" }
    let(:needs_updating) { [2, 4] }
    let(:scheduled) { { 2 => 0, 4 => 15 } }

    it "finds the IDs that need updating" do
      expect(subject.keys).to match_array needs_updating
    end

    it "creates the update schedule" do
      expect(subject).to eq(scheduled)
    end
  end

  context "example 3", vcr: { cassette_name: "example3" } do
    let(:example_file) { "spec/fixtures/example3.txt" }
    # These are the show IDs that need updating
    let(:needs_updating) { File.read("spec/fixtures/example3-updates.txt").split.map(&:to_i) }

    it "finds the IDs that need updating" do
      expect(subject.keys).to match_array needs_updating
    end

    it "creates the update schedule" do
      total_to_return = 4597
      first_result = 0
      last_result = 3599

      expect(subject.count).to eq(total_to_return)
      expect(subject[1]).to eq(first_result)
      expect(subject[9999]).to eq(last_result)

      # check also that all values are less than 3600 seconds
      expect(subject.values.all? { |value| value < 3600 }).to be_truthy
    end
  end
end
