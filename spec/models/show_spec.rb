RSpec.describe Show, type: :model do
  describe '.last_updates_since' do
    let(:example_file) { "spec/fixtures/example1.txt" }

    before do
      # Seed database
      File.read(example_file).each_line do |line|
        id, quantity, last_update = line.split(",")
        Show.create!(id: id, quantity: quantity, last_update: last_update)
      end
    end

    it 'returns shows updated within the given timeframe' do
      result = Show.last_updates_since(3600)

      expect(result.map(&:id)).to eq([1, 2])
    end

    it 'returns an empty result if no shows are updated within the given timeframe' do
      result = Show.last_updates_since(5.minutes.ago)

      expect(result).to be_empty
    end

    it 'returns all shows if seconds is nil' do
      Show.update_all(last_update: nil)

      result = Show.last_updates_since(3600)

      expect(result.map(&:id)).to match_array([1, 2, 3])
    end
  end
end
